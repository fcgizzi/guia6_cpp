// librerias
#include <iostream>
using namespace std;

// clase
#include "Ordena.h"

// TRUE y FALSE
#define TRUE 0
#define FALSE 1

Ordena::Ordena(){}

// funcion ordena QUICKSORT
void Ordena::ordena_quicksort(int a[], int n){
	// se toma un valor y los menores a este van a la izquierda y los mayores
	// a la derecha
	// se utilizan topes, inicio, final y posición, también auxuliar
	int tope, ini, fin, pos, der, izq, aux, band;
	int menor[100];
	int mayor[100];
	tope = 0;
	menor[tope] = 0;
	mayor[tope] = n-1;
	
	// ciclo while hasta que se cumpla condición
	while(tope >= 0){
		// se guardan valores y a tope se le resta uno
		ini = menor[tope];
		fin = mayor[tope];
		tope = tope - 1;
		// reduce
		izq = ini;
		der = fin;
		pos = ini;
		band = TRUE;
		// ciclo while
		while(band == TRUE){
			while((a[pos] <= a[der]) && (pos != der))
				der = der - 1;
				// si la posicion es igual a der band se hace falso
				if(pos == der){
					band = FALSE;
				}
				else{
					// guarda en aux y se cambian posiciones
					aux = a[pos];
					a[pos] = a[der];
					a[der] = aux;
					pos = der;
					
					// se repite ciclo
					while((a[pos] >= a[izq]) && (pos != izq))
						// se le suma 1 a izq
						izq = izq + 1;
						// si posicion es igual a izq band se hace falso
						if(pos == izq){
							band = FALSE;
						}
						else{
							// se cambian los valores y se guarda en aux
							aux = a[pos];
							a[pos] = a[izq];
							a[izq] = aux;
							pos = izq;
						}
				}
		}
		// si el inicio es menor que pos - 1 se le suma 1 al tope
		// la pila menor en tope será igual al inicial
		// la pila mayor en rope será igual a posición -1
		if(ini < (pos - 1)){
			tope = tope + 1;
			menor[tope] = ini;
			mayor[tope] = pos - 1;
		}
		// si valor fin es mayor a pos + 1 se le suma 1 al tope
		// la pila menor en tope será igual a la posición + 1
		// la pila mayor en tope será igual a valor final
		if(fin > (pos + 1)){
			tope = tope + 1;
			menor[tope] = pos + 1;
			mayor[tope] = fin;
		}	
	}
}

// funcion que ordena SELECCION
void Ordena::ordena_seleccion(int a[], int n){
	// buscar el menor dato de la lista y dejarlo en la primera posicion
	// despues buscar el segundo menor y dejarlo en la segunda posicion
	// y así sucesivamente
	int menor, k;
	
	// ciclo for para buscar los valores
	for(int i=0; i<=n-2; i++){
		// se guarda el valor y la posición
		menor = a[i];
		k = i;
		
		// ciclo para buscar la siguiente posición
		for(int j=i+1; j<=n-1; j++){
			// si la siguiente posicion es menor
			if (a[j] < menor) {
				// se guarda ese valor y la posicion
				menor = a[j];
				k = j;
			}
		}
		// se guardan los valores en la posicion
		a[k] = a[i];
		a[i] = menor;
	}
}

// funcion que imprime un vector que se llevaran al apartado de listas
void Ordena::imprimir(int a[], int n){
	cout << endl;
	// ciclo for que recorre la lista creada
	for(int i=0; i<n; i++){
		// imprime sus valores y la posicion en la que está
		cout << "\t a[" << i <<"]=" << a[i] << " ";
	}
}
