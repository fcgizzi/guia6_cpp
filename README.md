# Métodos de ordenamiento interno (QUICKSORT y SELECCION)

Se solicita un programa que llene arreglos unidimensionales de manera aleatoria y con una cantidad ingresada por el usuario, también debe ordenar las listas con dos métodos de ordenamiento interno, finalmente se indica la cantidad de tiempo que tardó cada proceso de ordenammiento en ordenar de manera creciente los arreglos.

# Compilación
- Se escribe `make` en la terminal, si no se tiene, se instala con anterioridad ingresando `sudo apt install make` en la terminal

- Si no se tiene make, se usa `g++ main.cpp Arbol.cpp Grafo.cpp -o main` para compilar

# Ejecución

Para ejecutar el programa el programa se ingresa `./main` en la terminal, además de esto se agrega la cantidad de datos que el usuario quiere que sean trabajados (1- 1000000) junto con un vector (n, s)

# Programa

El programa trabaja completamente solo, solo hay un ingreso de valores, el resto lo imprime al trabajar, entrgando mediciones de tiempo que tardó cada procedimiento de ordenamiento.

# Requisitos

- Sistema operativo Linux
- Make instalado 

# Construido con
- Linux
- Geany
- Lenguaje c++

# Autor
- Franco Cifuentes Gizzi