// librería
#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

// constructores
class Ordena {
    private:
    public:
        // genera constructor de la clase
        Ordena();
        //funciones
        // ordena con método quicksort
        void ordena_quicksort(int a[], int n);
        // ordena con método selección
        void ordena_seleccion(int a[], int n);
        // función que imprime los vectores y posición
        void imprimir(int a[], int n);
};
#endif
