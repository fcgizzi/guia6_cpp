/*
 * Compilación: $ make
 * Ejecución: $ ./main [valor que desee entre 1 y 1000000]
 */

// librerias
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace std;

//clase
#include "Ordena.h"

// limites  
#define LIMITE_SUPERIOR 1000000
#define LIMITE_INFERIOR 1

// imprime cuando se es ingresado valor con vector n
void listas_n(Ordena o, int a[], int b[], int n){
	
	// variables de tiempo
	clock_t time1, time2, time3, time4;
	double msecs, msecs2;
	
	// QUICKSORT
	// se toma el tiempo antes de ordenar
	// llama a clase para ordenar
	// se obtiene un tiempo al ordenar
	time1 = clock();
	o.ordena_quicksort(a, n);
	time2 = clock();
	// se calculan los milisegundos
	msecs = (double)(time2 - time1) / CLOCKS_PER_SEC;

	// SELECCION
	// se toma el tiempo antes de ordenar
	// se ordena
	// se toma el tiempo al terminar de ordenar
	time3 = clock();
	o.ordena_seleccion(b, n);
	time4 = clock();
	// se calculan los milisegundo
	msecs2 = (double)(time4 - time3) / CLOCKS_PER_SEC;
	
	// se imprime la info obtenida de timepos
	cout << "\n\t-----------------------------------------------" << endl;
	cout << "\t| >> Metodo            | >> Tiempo            |" << endl;
	cout << "\t-----------------------------------------------" << endl;
	cout << "\t  >> Quicksort         |  " << msecs * 1000.0 << " milisegundos" << endl;
	cout << "\t  >> Seleccion         |  " << msecs2 * 1000.0 << " milisegundos" << endl;
	cout << "\t-----------------------------------------------" << endl;
}

// función imprime cuendo se ingresa valor con vector s
// además lista original y lista ordenada 
void listas_s(Ordena o, int a[], int b[], int n){
	
	// variables de tiempo
	clock_t time1, time2, time3, time4;
	double msecs, msecs2;
	
	// la lista original
	cout << "\n\t-----------------------------------------------" << endl;
	cout << "\n\t>> Lista original: ";
	o.imprimir(a, n);
	// QUICKSORT

	// se toma el tiempo antes de ordenar con el metodo Quicksort
	time1 = clock();
	// se ordena con el metodo Quicksort
	o.ordena_quicksort(a, n);
	// se toma el tiempo al terminar de ordenar
	time2 = clock();
	// se calculan los milisegundos
	msecs = (double)(time2 - time1) / CLOCKS_PER_SEC;

	// SELECCION
	// se toma el tiempo antes de ordenar con el metodo Seleccion
	time3 = clock();
	// se ordena la lista con el metodo se seleccion
	o.ordena_seleccion(b, n);
	// se toma el tiempo al terminar de ordenar
	time4 = clock();
	// se calculan los milisegundo
	msecs2 = (double)(time4 - time3) / CLOCKS_PER_SEC;
	
	// se imprime la info obtenida de timepos
	cout << "\n\n\t-----------------------------------------------" << endl;
	cout << "\t| >> Metodo            | >> Tiempo            |" << endl;
	cout << "\t-----------------------------------------------" << endl;
	cout << "\t  >> Quicksort         |  " << msecs * 1000.0 << " milisegundos" << endl;
	cout << "\t  >> Seleccion         |  " << msecs2 * 1000.0 << " milisegundos" << endl;
	cout << "\t-----------------------------------------------" << endl;
	
	// se imprimen las listas ordenadas con su metodo de ordenamiento
	cout << "\n\t>> Seleccion: ";
	o.imprimir(b, n);
	
	cout << "\n\t>> Quicksort: ";
	o.imprimir(a, n);
	cout << "\n\t-----------------------------------------------" << endl;
}

// creación de lista con los valores aleatorios
void crea_lista(int n, int a[], int b[]){
	int elemento;
	srand (time(NULL));
	// se van llenando las listas con numeros aleatorios entre 1 y 1000000
	for(int i=0; i<n; i++){
		elemento = (rand() % LIMITE_SUPERIOR) + LIMITE_INFERIOR;
		a[i] = elemento;
		b[i] = elemento;
	}
}

// función menú
int main(int argc, char *argv[]){
	
	// se lee lo ingresado en la terminal y se separa el int del vector 
	int n = atoi(argv[1]);
	
	// while para que el número ingresado esté dentro de los límites
	while(n > 1000000 or n < 1){
		cout << "\t>> el número ingresado debe estár dentro de los parámetros (1 - 1000000)";
		cin >> n;
	}
	// se guarda la letra
	char* ver = argv[2];
	
	// lista para cada método
	int a[n];//QUICKSORT
	int b[n];//SELECCION
	
	// se llama a la clase
	Ordena o = Ordena();
	
	// se llama a la función que crea listas con números aleatorios
	crea_lista(n, a, b);
	
	// para valor ingresado con s
	if(ver[0] == 's' or ver[0] == 'S'){
		// se imprimen listas e información de timepos
		listas_s(o, a, b, n);
	}
	// para valor ingresado con n
	else if(ver[0] == 'n' or ver[0] == 'N'){
		// se imprime info de tiempos sin listas
		listas_n(o, a, b, n);
	}
	return 0;
}
